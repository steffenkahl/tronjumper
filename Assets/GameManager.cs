﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = System.Random;

public class GameManager : MonoBehaviour
{
    public int points = 0;
    
    public TMP_Text scoretext = null;

    public GameObject fruitPrefab;

    private void Start()
    {
        createFruit();
    }

    private void Update()
    {
        scoretext.text = "SCORE: " + points.ToString();
    }

    public void eatFruit()
    {
        points += 10;
        createFruit();
    }

    private void createFruit()
    {
        Random r = new Random();
        Vector3 targetPos = new Vector3(r.Next(-50,50), r.Next(-25, 25), 0);

        Instantiate(fruitPrefab, targetPos, Quaternion.identity);
    }
}
