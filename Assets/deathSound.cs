﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathSound : MonoBehaviour
{
    private static deathSound instance = null;
    public static deathSound Instance {
        get { return instance; }
    }
    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
