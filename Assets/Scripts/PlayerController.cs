﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float gravity = 0.5f;
    private float velocity = 0f;
    public float speed = 25f;
    public float jumpPower = 10f;
    private bool inputJump = false;
    private Vector2 moveDirection = Vector2.zero;
    private CharacterController characterController;
    public GameManager gameManager;
    public AudioSource collectSound;
    public AudioSource deathSound;

    [SerializeField] private Animator eyeAnimator;
    // Start is called before the first frame update
    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    private void Update()
    {
        InputCheck();
        Move();
    }

    private void InputCheck()
    {
        velocity = Input.GetAxis("Horizontal") * speed;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            inputJump = true;
            eyeAnimator.SetTrigger("Jump");
        }
        else
        {
            inputJump = false;
        }
        
        eyeAnimator.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
    }

    private void Move()
    {
        if (characterController.isGrounded)
        {
            moveDirection.y = 0;
            if (inputJump)
                moveDirection.y = jumpPower;
        }
        moveDirection.x = velocity;
        moveDirection.y -= gravity;
        characterController.Move(moveDirection * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("death"))
        {
            StartCoroutine(Death());
        }
        else if(other.CompareTag("fruit"))
        {
            collectSound.Play();
            gameManager.eatFruit();
            Destroy(other.gameObject);
        }
    }
    
    IEnumerator Death()
    {
        deathSound.Play();
        yield return new WaitForSeconds(deathSound.clip.length);
        if (PlayerPrefs.GetInt("highscore") < gameManager.points)
        {
            PlayerPrefs.SetInt("highscore", gameManager.points);
            Debug.Log("Set Highscore to: " + gameManager.points);
            
        }
        SceneManager.LoadScene("MainMenu");
    }
}
