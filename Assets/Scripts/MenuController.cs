﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public TMP_Text highscoreText = null;

    private void Start()
    {
        updateHighscore();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void QuitGame()
    {
        Debug.Log("GAME QUITTING");
        Application.Quit();
    }

    public void updateHighscore()
    {
        highscoreText.text = "HIGHSCORE: " + PlayerPrefs.GetInt("highscore").ToString();
    }

    public void resetHighscore()
    {
        PlayerPrefs.SetInt ("highscore", 0);
        highscoreText.text = "HIGHSCORE: " + PlayerPrefs.GetInt("highscore").ToString();
    }

    public void showCredits()
    {
        SceneManager.LoadScene("Credits");
    }
}
