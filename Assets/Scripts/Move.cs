﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Move : MonoBehaviour
{
    //Counter
    [SerializeField] private int stepCount = 0;
    public int stepMax = 0;
    
    //KeyCodes
    public KeyCode upKey;
    public KeyCode downKey;
    public KeyCode rightKey;
    public KeyCode leftKey;

    //Speed
    public float speed = 16f;
    
    //Wall Prefab
    public GameObject wallPrefab;
    
    // Current Wall
    private Collider2D wall;

    // Last Wall's End
    private Vector2 lastWallEnd;

    // Start is called before the first frame update
    private void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
        spawnWall();
    }

    // Update is called once per frame
    private void Update()
    {
        stepExecute();
        
        if (Input.GetKeyDown(upKey)) {
            GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
            spawnWall();
        }
        else if (Input.GetKeyDown(downKey)) {
            GetComponent<Rigidbody2D>().velocity = -Vector2.up * speed;
            spawnWall();
        }
        else if (Input.GetKeyDown(rightKey)) {
            GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
            spawnWall();
        }
        else if (Input.GetKeyDown(leftKey)) {
            GetComponent<Rigidbody2D>().velocity = -Vector2.right * speed;
            spawnWall();
        }
    }
    
    private void spawnWall() {
        // Save last wall's position
        lastWallEnd = transform.position;

        // Spawn a new Lightwall
        GameObject g = Instantiate(wallPrefab, transform.position, Quaternion.identity);
        wall = g.GetComponent<Collider2D>();
    }

    private void stepExecute()
    {
        stepCount += 1;
        if (stepCount >= stepMax)
        {
            spawnWall();
            stepCount = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag( "vwall"))
        {
            Random r = new Random();
            int randomNumber = r.Next(0, 2);
            Debug.Log(randomNumber);
            if (randomNumber < 1)
            {
                GetComponent<Rigidbody2D>().velocity = -Vector2.up * speed;
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
            }
        }
        else if (other.gameObject.CompareTag("hwall"))
        {
            Random r = new Random();
            int randomNumber = r.Next(0, 2);
            Debug.Log(randomNumber);
            if (randomNumber < 1)
            {
                GetComponent<Rigidbody2D>().velocity = -Vector2.right * speed;
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
            }
        }
    }
}
