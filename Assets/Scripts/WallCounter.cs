﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCounter : MonoBehaviour
{
    public int lifeDuration = 10;
    private int lifeLeft;
    
    // Start is called before the first frame update
    void Start()
    {
        lifeLeft = lifeDuration;
    }

    // Update is called once per frame
    void Update()
    {
        lifeLeft -= 1;

        if (lifeLeft <= 0)
        {
            Destroy(gameObject);
        }
    }
}
